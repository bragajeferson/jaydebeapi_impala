import jaydebeapi

conn = jaydebeapi.connect(jclassname="com.cloudera.impala.jdbc41.Driver",
                          url="jdbc:impala://localhost:21050/default;principal=impala/localhost@EXEMPLO.COM;KrbHostFQDN=localhost;KrbServiceName=impala;AuthMech=1;SSL=1;SSLTrustStore=C:/Users/exemplo/cert.jks;AllowSelfSignedCerts=1",
                          driver_args={"UID":"impala","PWD":"impala","LogLevel":"6"},
                          jars="C:/Users/exemplo/ImpalaJDBC41.jar",)
curs = conn.cursor()
curs.execute("select * from table")
print(curs.fetchall())
curs.close()
conn.close()
