# Conexão Python no Impala Kerberizado e SSL usando JDBC Driver

## Pré-requisitos
* Python 3+
* jaydebeapi
```
pip install jaydebeapi
```
* MIT Kerberos

Comando para pegar o ticket: kinit -kt <path_to_keytab> principal

Exemplo:
```
kinit -kt impala.keytab impala/localhost@EXEMPLO.COM
```

## Exemplo

```
import jaydebeapi

conn = jaydebeapi.connect(jclassname="com.cloudera.impala.jdbc41.Driver",
                          url="jdbc:impala://localhost:21050/default;principal=impala/localhost@EXEMPLO.COM;KrbHostFQDN=localhost;KrbServiceName=impala;AuthMech=1;SSL=1;SSLTrustStore=C:/Users/exemplo/cert.jks;AllowSelfSignedCerts=1",
                          driver_args={"UID":"impala","PWD":"impala"},
                          jars="C:/ImpalaJDBC41.jar",)
curs = conn.cursor()
curs.execute("select * from table")
print(curs.fetchall())
curs.close()
conn.close()
```